import React from 'react';
import './App.css';
import Servicos from './Components/Servicos'

function App() {
  return (
    <div className="App">
      <Servicos/>
    </div>
  );
}

export default App;
